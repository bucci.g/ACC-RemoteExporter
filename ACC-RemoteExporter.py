import subprocess
from ion.plugin import *

import json


class ACC-RemoteExporter(IonPlugin):
    version = "1.0.1.2"


    # DEST_USER = 'hexe'
    # DEST_HOST = '10.111.98.131'
    # DEST_FOLDER = '/Volumes/Transcend/tmp'

    DEST_USER = 'mbarbie4'
    DEST_HOST = '192.168.122.1'
    DEST_PORT = '2201'
    DEST_FOLDER = '~/tmp'





    def launch(self, data=None):

        # src_file = "../../IonCodeTag_0103_rawlib.bam"

        output = ""

        # Load barcode data
        with open("barcodes.json", 'r') as fh:
            barcodes = json.load(fh)

        # Summarize content of the run
        output += "Barcodes found:\n"
        print("Barcodes found:")

        files_to_transfer = list()

        for bc in barcodes:
            files_to_transfer.append(barcodes[bc]['bam_filepath'])
            output += "{} [{}]\n".format(bc, barcodes[bc]['bam_filepath'])
            print("{} [{}]".format(bc, barcodes[bc]['bam_filepath']))


        output += "Target region file: {}\n".format(barcodes[bc]['target_region_filepath'])
        print("Target region file: {}".format(barcodes[bc]['target_region_filepath']))

        output += "Hotspot region file: {}\n".format(barcodes[bc]['hotspot_filepath'])
        print("Hotspot region file: {}".format(barcodes[bc]['hotspot_filepath']))

        # output = subprocess.check_output(['ls', '-l'])
        # output += subprocess.check_output(['whoami'])
        # output += subprocess.check_output(['pwd'])
        # output += subprocess.check_output(['hostname'])

        # output += subprocess.check_output([
        #     'rsync',
        #     '--append-verify', # allows to partially transfer files
        #     "-e 'ssh -p {}'".format(self.DEST_PORT),
        #     src_file,
        #     '{}@{}:{}'.format(self.DEST_USER, self.DEST_HOST, self.DEST_FOLDER)
        # ])

        for src_file in files_to_transfer:
            print("INFO: Transferring file {}".format(src_file))
            output += subprocess.check_output(
                "rsync --append-verify -e 'ssh -p {}' {} {}@{}:{}".format(self.DEST_PORT, src_file, self.DEST_USER, self.DEST_HOST, self.DEST_FOLDER), stderr=subprocess.STDOUT, shell=True)

        print(output)

        with open("status_block.html", "w") as html_fp:
            html_fp.write("<html><body><pre>")
            html_fp.write(output)
            html_fp.write("</pre></body></html>")

        # Apparently, you have to return True
        # to consider the Plugin execution successful
        return True


if __name__ == "__main__":
    PluginCLI()
