# ACC-RemoteExporter

A plugin for the Torrent Suite which allows to export bam files to a remote location

## Overview

This plugin uses `rsync` to transfer the _raw_ `.bam` files produced by a _run_ to a remote server.
By using option `--append-verify` it ensures that if the transfer is interrupted for any reason, the next time the plugin is launched (for the same run) the transfer is resumed so that it is not necessary to start over.

## Setup

Before the plugin can be used, it is necessary to setup the connection between the machine where those files are located and the remote server where they will be uploaded.

### Identifying the machine

First of all, it is necessary to identify _where_ the machine where the files for every run are saved.

As a matter of fact, raw `.bam` files are **not** directly accessible from the machine which runs the _Torrent Browser_; they actually are in a _virtual machine_ which runs on the physical machine (the S5 itself).

However, it might not be possible to access this virtual machine directly from a PC connected to the same network; it is necessary _first_ to log into the physical machine (the one whose IP address you type in the internet browser to access the Torrent Browser), and from there log into the virtual machine.
The IP address of such virtual machine can (probably) be found by looking at that `/etc/hosts` file in the physical machine.

For instance:

```shell-session
matteo@macbook:~$ ssh ionadmin@10.111.133.192
Password:

ionadmin@S5-00399:~$ cat /etc/hosts
127.0.0.1 S5-00399 localhost.localdomain localhost
192.168.122.249 tsvm localhost.localdomain localhost

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
...

ionadmin@S5-00399:~$ ssh ionadmin@192.168.122.249
Password:

ionadmin@tsvm-S5-00399:~$
```

### Generate ssh keys for user `ionian`

Once you are logged into the virtual machine, it is necessary to setup the passwordless access from there to the remote server where the files will be stored by generating the keys.
However, the user which actually runs the Torrent Browser (and therefore its plugins) is not the one you used to log into the machine (`ionadmin`), it is a different one, `ionadmin`.

Because of the way the user is configured however, it is not possible to open a shell with that user (the default shell for `ionian` is set to be `/bin/false` in order to prevent direct access). It is therefore necessary to use `sudo` to issue the following commands:


```shell-session
ionadmin@tsvm-S5-00399:~$ sudo -u ionian ssh-keygen -t rsa
Password:

Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/ionian/.ssh/id_rsa.
Your public key has been saved in /home/ionian/.ssh/id_rsa.pub.
The key fingerprint is:
73:d0:e9:0a:5d:a3:3f:78:33:5d:0d:fe:e4:f4:25:39 ionian@tsvm-S5-00399
The key's randomart image is:
+--[ RSA 2048 ]----+
|                 |
|         . .     |
|        . =   .  |
|       . = . . + |
|      . S o   E =|
|       . * . . Bo|
|        o * .   +|
|         . +     |
|                 |
+-----------------+
```

This way a public/private key pair is generated and saved in folder `/home/ionian/.ssh`.

### Add the **public** key to the list of authorized keys in the remote machine

Now you must add the **public** key you just generated to the list of authorized keys in the remote machine.
To do so, copy _the content_ of file `/home/ionian/.ssh/id_rsa.pub` (on the virtual machine), which will look something like this:

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC/2qLebQmZGtEFwEirNGU
+IrbfRXU5BR+2dm6q8Fvhs7GJFzlBgSM433a6G7yi2qfxfbu2s2W55mFTQG
mT57NrNr7utLxqyZVRgHoYfcwIKbWCQhVGRoXWDmLgaQW/62s8svDyUdMdf
MnFju0W+7OupESV24Ju6WioflV2VuDBJKuMOhrZC6m/9gccOJuoM/1zS0Rw
oup1Jt4wtAssCF3AU6D3r6JPoKxZ/PQTLlGq6NwTRuyCJAp+VVqD0Qlqj9y
QaLagYMjfaPvdmtP3VDKQoGuCKMrMiqKVJW4V1muhxFduauqoDWFYLLZqKP
Bakh+UlVhdo3KymgfITSAGKEUv ionian@tsvm
```

into file `~/.ssh/authorized_keys` in the home folder of the user which will be used to actually perform the transfer (see Section [Plugin Configuration](#configuration)).
Caution: the actual key will probably consist of more rows than the one displayed above. Also, be careful not to delete or edit any other lines in file `~/.ssh/authorized_keys` or you might risk preventing other people to perform the transfer.

To check if everything is setup correctly, try command

```shell-session
ionadmin@tsvm-S5-00399:~$ sudo -u ionian ssh <REMOTE_SERVER_ADDRESS>
```

It might prompt you with a message asking whether you want to add the address to the list of known hosts (or something like that), answer `yes` and press enter. You should be able to log onto the remote server without being asked for the password.
If you disconnect and try again, this time you should be able to directly log  onto the server without being prompted again.

```shell-session
ionadmin@tsvm-S5-00399:~$ sudo -u ionian ssh <REMOTE_SERVER_ADDRESS>

user@remote_server:~$
```

## Plugin configuration and installation

Before installing the plugin, it is necessary to make minor modifications to the code.

### Configuration

Download the Plugin archive, extract it somewhere and open file `RemoteExporter.py` with any text editor. Around row 15 (just after the _class_ declaration) you should see four rows which determine the connection parameters to the remote server.

 * `DEST_USER` is the user mentioned earlier which will actually be used for the connection.
 * `DEST_HOST` is the address of the remote server, for instance `login.pico.cineca.it`
 * `DEST_PORT` is the port used for the connection. Unless otherwise specified it should be the default ssh port, that is **22**.
 * `DEST_FOLDER` is the folder on the remote server where the files will actually be saved.

Change these four parameters according to your setup, then save the file and zip the whole folder again.

### Installation

After changing the parameters as explained above, you may install the plugin using the dedicate section in the _Torrent Browser_.

![plugin installation](plugin_installation.png "Plugin installation")


From the gear icon in the upper right corner of the main page, click on _Plugins_ and then on _Install or Upgrade Plugin_. Choose the archive of the plugin and click on _Upload and Install_.

Before the plugin can be used, it is necessary to setup the _passwordless_ access to the remote server from the virtual machine where raw `.bam` files actually are stored. See section [Setup](#setup) for more information about this step.
