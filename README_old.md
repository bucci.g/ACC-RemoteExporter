# TSRemoteExporter

A plugin for the Torrent Suite which allows to export bam files to a remote location

## Overview

Questo plugin trasferisce i file relativi ad una corsa su un server remoto 
utilizzando `rsync`. Utilizza l'opzione `--append-verify` per fare in modo che 
nel caso il trasferimento venga interrotto per una qualunque ragione, rilanciare il plugin
faccia riprendere il caricamento dei file dal punto in cui si era rimasti.
Questo permette sia di evitare di dover ricaricare _tutti_ i file ogni volta,
sia di dover riprendere _dall'inizio_ il caricamento di un file magari
particolarmente grosso.

Per ogni corsa vengono caricati i file **.bam** _raw_.

## Setup

Prima che il plugin possa essere usato, deve essere configurata la connessione
dalla macchina dove fisicamente risiedono i dati verso il server remoto
(cineca).

### Identificazione macchina

Come prima cosa, e' necessario individuare tale macchina. Includo questa parte
per evitare potenziali mal di testa per chi non ha familiarita' con questo tipo
di strumenti (categoria di cui, per inciso, faccio parte [Matteo Barbieri]).

Se non ho capito male, i file raw (i `.bam`) **non** sono direttamente
accessibili dalla macchina su cui gira il _Torrent Browser_ (per intenderci, quella
il cui indirizzo IP digitate nella barra del browser per poter accedere alle
corse).
Sono in verita' accessibili all'interno di una _macchina virtuale_ che assumo
giri sullo stesso server del Torrent Browser. Non e' possibile pero' (credo)
accedere direttamente tramite `ssh` a quella macchina virtuale, neanche se si
e' nella stessa sottorete. E' necessario _prima_ entrare nella macchina "reale"
e poi da li' fare ssh verso la macchina virtuale. E' altresi' necessario sapere
l'indirizzo IP di questa macchina virtuale; dovreste trovarlo spulciando nel
file `/etc/hosts` della macchina virtuale.

Esempio pratico:

```shell-session
matteo@macbook:~$ ssh ionadmin@10.111.133.192
Password:

ionadmin@S5-00399:~$ cat /etc/hosts
127.0.0.1 S5-00399 localhost.localdomain localhost
192.168.122.249 tsvm localhost.localdomain localhost

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
...

ionadmin@S5-00399:~$ ssh ionadmin@192.168.122.249
Password:

ionadmin@tsvm-S5-00399:~$
```

### Creazione delle chiavi per l'utente `ionian`

A questo punto e' necessario creare una coppia di chiavi per l'accesso
passwordless verso il server cineca. L'utente per cui dovete crearle pero' non
e' lo stesso con cui vi siete loggati (`ionadmin`) ma un altro, `ionian`, che
e' quello con cui vengono eseguiti di fatto gli script dei plugin (l'ho
scoperto a tentativi).

Non e' possibile pero' loggarsi sulla macchina direttamente come quell'utente a
causa del modo in cui e' configurata (la shell di default per quell'utente e'
settata a `/bin/false`, per prevenire l'accesso diretto). E' necessario usare
quindi `sudo` per "spacciarsi" come `ionian`.

```shell-session
ionadmin@tsvm-S5-00399:~$ sudo -u ionian ssh-keygen -t rsa
Password:

Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/ionian/.ssh/id_rsa.
Your public key has been saved in /home/ionian/.ssh/id_rsa.pub.
The key fingerprint is:
73:d0:e9:0a:5d:a3:3f:78:33:5d:0d:fe:e4:f4:25:39 ionian@tsvm-S5-00399
The key's randomart image is:
+--[ RSA 2048 ]----+
|                 |
|         . .     |
|        . =   .  |
|       . = . . + |
|      . S o   E =|
|       . * . . Bo|
|        o * .   +|
|         . +     |
|                 |
+-----------------+
```

### Copiare la chiave **pubblica** all'interno del file sulla macchina del cineca

A questo punto e' necessario aggiungere l'identita' appena creata alla lista di
quelle autorizzate nella macchina remota (quella del cineca). Per fare questo,
copiate il contenuto del file `/home/ionian/.ssh/id_rsa.pub` (dovete essere
loggati sulla macchina virtuale), che assomigliera' a qualcosa del genere:

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC/2qLebQmZGtEFwEirNGU
+IrbfRXU5BR+2dm6q8Fvhs7GJFzlBgSM433a6G7yi2qfxfbu2s2W55mFTQG
mT57NrNr7utLxqyZVRgHoYfcwIKbWCQhVGRoXWDmLgaQW/62s8svDyUdMdf
MnFju0W+7OupESV24Ju6WioflV2VuDBJKuMOhrZC6m/9gccOJuoM/1zS0Rw
oup1Jt4wtAssCF3AU6D3r6JPoKxZ/PQTLlGq6NwTRuyCJAp+VVqD0Qlqj9y
QaLagYMjfaPvdmtP3VDKQoGuCKMrMiqKVJW4V1muhxFduauqoDWFYLLZqKP
Bakh+UlVhdo3KymgfITSAGKEUv ionian@tsvm
```

nel file `~/.ssh/authorized_keys` nella home del vostro utente (o meglio,
dell'utente che verra' scelto per fare il trasferimento, dovrebbe essere unico
per tutti). Attenzione: tutta la chiave consiste di una sola riga, anche se qui per evidenti
ragioni l'ho spezzata su piu' righe.

## Installazione

Il plugin si installa normalmente utilizzando la sezione dedicata sul Torrent
Browser.

E' necessario che sia configurato l'accesso _passwordless_ (usando i file
chiave _rsa_) dalla macchina su cui fisicamente risiedono i file verso il server remoto 
(il cineca). Vedere la sezione [Setup](#setup) per ulteriori riguardo questo
step.
